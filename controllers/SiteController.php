<?php

namespace app\controllers;

use app\models\Links;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
	            'view' => 'error.twig'
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index.twig');
    }


	public function actionRedirect($code)
	{
		if(($url = Yii::$app->cache->getOrSet('link:'.$code, function () use ($code) {
			return Links::findOne(['code' => $code])->url;
		}))) {
			$this->redirect($url);
		} else {
			throw new NotFoundHttpException('Link not found');
		}
	}
}
