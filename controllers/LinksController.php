<?php
/**
 * Created by PhpStorm.
 * User: stivvi
 * Date: 08.08.2019
 * Time: 20:59
 */

namespace app\controllers;

use yii\rest\ActiveController;

class LinksController extends ActiveController
{
	public $modelClass = 'app\models\Links';
}