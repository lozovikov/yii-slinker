<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "links".
 *
 * @property int $id
 * @property string $url
 * @property string $code
 */
class Links extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links';
    }

	public function fields()
	{
		return [
			'code',
			'url' => function($model){return Url::toRoute(['site/redirect', 'code' => $model->code], true);}
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'code'], 'required'],
            [['url'], 'string'],
	        [['url'], 'url'],
            [['code'], 'string', 'max' => 5],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'code' => 'Code',
        ];
    }

    public function beforeValidate()
    {
    	$this->code = $this->generateCode();
        return parent::beforeValidate();
    }

    protected function generateCode()
    {
	    $code = Yii::$app->security->generateRandomString(5);
	    if(self::findOne(['code' => $code])) {
	    	return $this->generateCode();
	    }
	    return $code;
    }
}
