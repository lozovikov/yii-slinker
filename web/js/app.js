$(document).ready(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('button:not(.disabled).js-get-short-url').on('click', function() {
		let $button = $(this);
		$button.addClass('disabled').attr("disabled", true);
		let $form = $button.closest('form');
		let url = $("input[name=url]").val();
		$.ajax({
			url: $form.attr('action'),
			type: 'POST',
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: JSON.stringify({'url': url}),
			success: function(data){
				$('.js-form-response').html('<div class="alert alert-success"></div>');
				$('div.alert').html(data.url);
				let url = new URL(data.url);
				$button.removeClass('disabled').removeAttr("disabled");
			},
			error: function (data) {
				$('.js-form-response').html('<div class="alert alert-danger"></div>');
				let error = data.responseJSON ? data.responseJSON[0].message : '';
				$('div.alert').html(error);
				$button.removeClass('disabled').removeAttr("disabled");
			}
		});
		return false;
	});
});
