<?php

use yii\db\Migration;

/**
 * Class m190808_095613_links
 */
class m190808_095613_links extends Migration
{

    public function up()
    {
	    $this->createTable('links', [
		    'id' => $this->primaryKey(),
		    'url' => $this->text()->notNull(),
		    'code' => $this->string(5)->notNull()->unique(),
	    ]);
    }

    public function down()
    {
        $this->dropTable('links');
    }
}
